@extends('layouts.landing')

    @section('css')
    
    @endsection

@section('content')
    <!-- bradcam_area_start -->
    <div class="bradcam_area breadcam_bg_1">
        <h3>Hasil Pencarian</h3>
    </div>
    <!-- bradcam_area_end -->

    <!-- offers_area_start -->
    <div class="offers_area padding_top">
        <div class="container">
        @include('notification')
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title text-center mb-100">
                        <span>Featured Rooms</span>
                        <h3>Choose a Better Room</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($featured as $r)
                <div class="col-xl-4 col-md-4">
                    <div class="single_offers">
                        <div class="about_thumb">
                            <img src="{{asset('montana-master/img/offers/1.png')}}" alt="">
                        </div>
                        <h3>{{$r->room_name}}</h3>
                        <ul>
                            <li>Luxaries condition</li>
                            <li>3 Adults & 2 Children size</li>
                            <li>Sea view side</li>
                        </ul>
                        <a href="{{route('room-book',['id'=>$r->id])}}" class="book_now">book now</a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- offers_area_end -->

    <!-- features_room_startt -->
    <div class="features_room">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title text-center mb-100">
                        <span>All Rooms</span>
                        <h3>Choose a Better Room</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="rooms_here">
            @foreach($room as $r)
            <div class="single_rooms">
                <div class="room_thumb">
                    <img src="{{asset('montana-master/img/rooms/1.png')}}" alt="">
                    <div class="room_heading d-flex justify-content-between align-items-center">
                        <div class="room_heading_inner">
                            <span>Rp {{$r->price}}/night</span>
                            <h3>{{$r->room_name}}</h3>
                        </div>
                        <a href="{{route('room-book',['id'=>$r->id])}}" class="line-button">book now</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <!-- features_room_end -->

@endsection
    
    @section('js')

    @endsection