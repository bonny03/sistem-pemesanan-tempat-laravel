@extends('layouts.landing')

    @section('css')
    
    @endsection

@section('content')
    <!-- bradcam_area_start -->
    <div class="bradcam_area breadcam_bg">
        <h3>Your History Service</h3>
    </div>
    <!-- bradcam_area_end -->

	<!--================Blog Area =================-->
    <section class="blog_area section-padding">
        <div class="container">
        @include('notification')
            <div class="row">
                <div class="col-lg-8 mb-5 mb-lg-0">
                    <div class="blog_left_sidebar">
                        @foreach($book as $b)
                        <article class="blog_item">
                            <div class="blog_item_img">
                                <img class="card-img rounded-0" src="img/blog/single_blog_1.png" alt="">
                                <a href="{{route('room-book',['id'=>$b->room_id])}}" class="blog_item_date">
                                    <p>{{$b->room->room_name}}</p>
                                </a>
                            </div>

                            <div class="blog_details">
                                <a class="d-inline-block" href="single-blog.html">
                                    @if($b->status_pembayaran == false)
                                    <h2>Belum dibayar, silakan hubungi cs</h2>
                                    @elseif($b->status_pembayaran == true)
                                    <h2>Sudah dibayar</h2>
                                    @endif
                                </a>
                                <ul class="blog-info-link">
                                    <li><a href="#"><i class="fa fa-calendar-check-o"></i>Checkin = {{$b->checkin}}</a></li>
                                    <li><a href="#"><i class="fa fa-calendar-times-o"></i>Checkout = {{$b->checkout}}</a></li>
                                    <li><a href="#"><i class="fa fa-money"></i>Total Harga = Rp {{$b->total_harga}}</a></li>
                                </ul>
                            </div>
                        </article>
                        @endforeach

                        <nav class="blog-pagination justify-content-center d-flex">
                            <ul class="pagination">
                            {{ $book->links() }}
                                <!-- <li class="page-item">
                                    <a href="#" class="page-link" aria-label="Previous">
                                        <i class="ti-angle-left"></i>
                                    </a>
                                </li> -->
                                <!-- <li class="page-item">
                                    <a href="#" class="page-link"></a>
                                </li> -->
                                <!-- <li class="page-item active">
                                    <a href="#" class="page-link">2</a>
                                </li>
                                <li class="page-item">
                                    <a href="#" class="page-link" aria-label="Next">
                                        <i class="ti-angle-right"></i>
                                    </a>
                                </li> -->
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="blog_right_sidebar">
                        <aside class="single_sidebar_widget search_widget">
                            <form method="GET" action="{{route('search')}}">
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="text" name="search" class="form-control" placeholder='Search Keyword'
                                            onfocus="this.placeholder = ''"
                                            onblur="this.placeholder = 'Search Keyword'">
                                        <div class="input-group-append">
                                            <button class="btn" type="button"><i class="ti-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <button class="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn"
                                    type="submit">Search</button>
                            </form>
                        </aside>

                        <aside class="single_sidebar_widget post_category_widget">
                            <h4 class="widget_title">Category</h4>
                            <ul class="list cat-list">
                                @foreach($type as $t)
                                <li>
                                    <a href="{{route('type',$t->id)}}" class="d-flex">
                                        <p>{{$t->name}}</p>
                                        <p></p>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </aside>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================Blog Area =================-->

@endsection
    
    @section('js')

    @endsection