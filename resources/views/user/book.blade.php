@extends('layouts.landing')

    @section('css')
    
    @endsection

@section('content')
    <!-- bradcam_area_start -->
    <div class="bradcam_area breadcam_bg">
        <h3>Booking {{$room->room_name}}</h3>
    </div>
    <!-- bradcam_area_end -->
	<!-- Start Align Area -->
	<div class="whole-wrap">
		<div class="container box_1170">
			<div class="section-top-border">
				<div class="row">
					<div class="col-lg-8 col-md-8">
						<h3 class="mb-30">Booking this Room</h3>
						@include('validate')
                          {!! Form::open(['route'=>'room.store','method'=>'POST']) !!}
                            <input type="hidden" name="id" value="{{$room->id}}">
							<div class="mt-10">
								<input type="text" name="keperluan" placeholder="Tulis keperluan menyewa, contoh : menginap/pernikahan"
									onfocus="this.placeholder = ''" onblur="this.placeholder = 'Tulis keperluan menyewa, contoh : menginap/pernikahan'" required
									class="single-input">
							</div>
							<div class="mt-10">
								<input type="number" id="jumlah_ruangan" name="jumlah_ruangan" placeholder="Jumlah ruangan yang dipesan"
									onfocus="this.placeholder = ''" onblur="this.placeholder = 'Jumlah ruangan yang dipesan'" required
									class="single-input">
							</div>
							<div class="mt-10">
								<input type="number" name="jumlah_orang" placeholder="Jumlah orang"
									onfocus="this.placeholder = ''" onblur="this.placeholder = 'Jumlah orang'" required
									class="single-input">
							</div>
							<div class="mt-10">
							<label for="checkin">checkin <span id="cekin"></span></label>
							<input type="hidden" id="checkin" name="checkin" value="" >
								<!-- <input type="date" name="checkin" placeholder="Check In"
									onfocus="this.placeholder = ''" onblur="this.placeholder = 'Check In'" required
									class="single-input"> -->
							</div>
							<div class="mt-10">
							<label for="checkout">checkout <span id="cekout"></span></label>
							<input type="hidden" id="checkout"  name="checkout" value="" >
								<!-- <input type="date" name="checkout" placeholder="Check Out"
									onfocus="this.placeholder = ''" onblur="this.placeholder = 'Check Out'" required
									class="single-input"> -->
							</div>
							
							<table class="table mt-5 mb-5">
								<thead>
									<tr>
									<th scope="col"><button type="button" id="hapus" class="btn btn-info" onclick="resets()">reset</button></th>
									<!-- <th scope="col">First</th>
									<th scope="col">Last</th>
									<th scope="col">Handle</th> -->
									</tr>
								</thead>
								
								<tbody>
									@php $week=0; @endphp
									@for($i=0; $i<$num_days; $i++)
										@if($week == 0)
											<tr>
										@endif
											@php $hari = strtotime($dstart . "+ $i days");  @endphp
											@if(in_array(date('Y-m-d',$hari),$order))
											<td><button type="button" class="btn btn-danger" disabled>{{date('M d',$hari )}}</button></td>
											@else
											<td><button type="button" id="b-{{$i}}" class="btn btn-success" data-jml="{{$sisa[$i]}}" onclick="check( {{$i}},'{{date('Y-m-d',$hari)}}')">{{date('M d',$hari )}}({{ $sisa[$i] }})</button></td>
											@endif
										@if($week == 4)
											</tr>
										@endif
										@php 
										if($week == 4){
											$week = 0;
										}
										else{
											$week++;
										}
										@endphp
									@endfor
								</tbody>
							</table>


							
							<div class="mt-10">
								<input type="number" name="no_wa" placeholder="Nomor Whatsapp"
									onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nomor Whatsapp'" required
									class="single-input">
							</div>
                            <br>
                            <button type="submit" class="genric-btn success-border">Submit</button>
                            
                        </form>
					</div>
					<div class="col-lg-3 col-md-4 mt-sm-30">
						<div class="single-element-widget">
							<h3 class="mb-30">About {{$room->room_name}}</h3>
							<div class="switch-wrap d-flex justify-content-between">
								<p>Harga Ruangan : {{$room->price}}</p>
								
							</div>
							<div class="switch-wrap d-flex justify-content-between">
								<p>Fasilitas : {{$room->facility}}</p>
								
							</div>
							<div class="switch-wrap d-flex justify-content-between">
								<p>Room Type : {{$room->type->name}}</p>
								
							</div>
							<div class="switch-wrap d-flex justify-content-between">
								<p>Ukuran : {{$room->room_size}}</p>
								
							</div>
							<div class="switch-wrap d-flex justify-content-between">
								<p>Deskripsi : {{$room->description}}</p>
								
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Align Area -->
	
@endsection
    
    @section('js')
	<script>
		var start=0;end=0,stop=false;
		var total = {{$num_days}};
		
					
		function resets(){
			// alert();
			for (var index = 0; index < total; index++) {
					$("#b-"+index).attr('class', 'btn btn-success');
			}
			$('#checkout').val('');
			$('#checkin').val('');
			$('#cekin').text('');
			$('#cekout').text('');
			start=0;
			end=0
		}
		
		$("#jumlah_ruangan").change(function(){
			if($("#jumlah_ruangan").val() != ''){
				for (var index = start; index < end; index++) {
				if( $("#jumlah_ruangan").val() <= $("#b-"+index).data('jml') ){
					$("#b-"+index).attr('class', 'btn btn-primary');
				}
				else{
					stop=true;
					break;
				}
				}
				if(stop){
					resets();
					stop=false;
					alert('Maaf ruangan tidak cukup');
				}
			}
			if($("#jumlah_ruangan").val() == 0){
				$("#jumlah_ruangan").val(1)
			}
			
		});
		
		function check(id,date){
			if($("#jumlah_ruangan").val() == ''){
				$("#jumlah_ruangan").val(1);
			}
			if($('#checkin').val() == ''){
				$("#b-"+id).attr('class', 'btn btn-primary');
				$('#checkin').val(date);
				$('#cekin').text(date);
				start = id;
			}
			else{
				
				if($('#checkin').val() > date){
					$('#checkout').val($('#checkin').val());
					$('#cekout').text($('#checkin').val());
					$('#checkin').val(date);
					$('#cekin').text(date);
					for (var index = start; index <= end; index++) {
						$("#b-"+index).attr('class', 'btn btn-success');
					}
					end = start;
					start = id;
					
				}
				else{
					$('#checkout').val(date);
					$('#cekout').text(date);
					if(end > id){
						for (var index = start; index <= end; index++) {
							$("#b-"+index).attr('class', 'btn btn-success');
					    }
					}
					end = id;
				}
				$("#b-"+id).attr('class', 'btn btn-primary');
				
			}
			
			for (var index = start; index <= end; index++) {
				if( $("#jumlah_ruangan").val() <= $("#b-"+index).data('jml') ){
					$("#b-"+index).attr('class', 'btn btn-primary');
				}
				else{
					stop=true;
					break;
				}
			}
			if(stop){
				resets();
				stop=false;
				alert('Maaf ruangan tidak cukup');
			}
			
		}

	</script>
    @endsection