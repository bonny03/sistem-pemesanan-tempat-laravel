@extends('layouts.app')

@section('content')

<div class="container">
    <style type="text/css">
    .card-header {
        background-color: skyblue;
    }
    </style>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                  <div class="card-header">roomtype</div>
                      <div class="card-body">
                              @include('validate')
                          {!! Form::open(['route'=>'roomtype-admin.store','method'=>'POST']) !!}

                          <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right"><i class="fas fa-space-shuttle">roomtype Name</label></i>
                                 <div class="col-md-6">
                                 {!! Form::text('name',null,['class'=>'form-control']) !!}
                                 </div>
                           </div>

                          <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">roomtype Description</label>
                                 <div class="col-md-6">
                                 <textarea name="description" id="" class="form-control"></textarea>
                                 </div>
                           </div>



                               <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-2">
                                        <button type="submit" class="btn btn-success btn-sm">Add data</button>
                                       <a href="{{ route('roomtype-admin.index') }}" class="btn btn-info btn-sm">Back</a>
                                    </div>
                          </div>
                      </div>
                  </div>
            </div>
        </div>
    </div>
</div>
@endsection
