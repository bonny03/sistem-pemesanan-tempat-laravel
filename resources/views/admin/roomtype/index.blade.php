@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">roomtype</div>


                   <div class="card-body">
                       <a href="{{ route('roomtype-admin.create') }}" class="btn btn-danger btn-sm">Add roomtype</a>

                      <hr>
                      @include('notification')

                    <table class="table table-bordered" id="users-table">
                       <thead>
                         <tr>
                            <th>No</th>
                            <th>room type Name</th>
                            <th>Edit</th>
                            <th>Delete</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; ?>
                        @foreach ($roomtype as $item)


                        <tr>
                        <td>{{ $no }}</td>
                        <td>{{ $item->name }}</td>
                            <td><a href="{{ route('roomtype-admin.edit',$item->id)}}" class="btn btn-info btn-sm">Edit</a></td>
                            {!! Form::open([ 'route'=>['roomtype-admin.destroy',$item->id], 'method'=>'DELETE']) !!}
                            <td><button type="submit" name="submit" class="btn btn-danger btn-sm">Delete</button></td>
                            {!! Form::close() !!}


                        </tr>
                        <?php $no++; ?>

                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script>
$(function() {
$('#users-table').DataTable();
});
</script>
@endpush

