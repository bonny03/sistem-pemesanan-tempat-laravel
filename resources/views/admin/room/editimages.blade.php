@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                  <div class="card-header">Edit room</div>
                      <div class="card-body">

                          @include('validate')

                          {!! Form::model($room,['route'=>['edit-image-room-admin'],'method'=>'POST','enctype'=>'multipart/form-data']) !!}
                          <input type="hidden" name="id" value="{{$room->id}}">
                          <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Images</label>
                                 <div class="col-md-6">
                                 {!! Form::file('images',null,['class'=>'form-control']) !!}
                                 </div>
                           </div>
                            <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-2">
                                        <button type="submit" class="btn btn-success btn-sm">Update Data</button>
                                        <a href="{{ route('room-admin.index')  }}" class="btn btn-info btn-sm">Back</a>
                                    </div>
                          </div>
                      </div>
                  </div>
            </div>
        </div>
    </div>
</div>
@endsection
