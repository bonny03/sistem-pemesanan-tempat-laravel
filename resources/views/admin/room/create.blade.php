@extends('layouts.app')

@section('content')

<div class="container">
    <style type="text/css">
    .card-header {
        background-color: skyblue;
    }
    </style>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                  <div class="card-header">Room</div>
                      <div class="card-body">
                              @include('validate')
                          {!! Form::open(['route'=>'room-admin.store','method'=>'POST','enctype'=>'multipart/form-data']) !!}
                          <form method="POST" action="{{route('room-admin.store')}}" enctype="multipart/form-data">
                          <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right"><i class="fas fa-space-shuttle">Room Name</label></i>
                                 <div class="col-md-6">
                                 {!! Form::text('room_name',null,['class'=>'form-control']) !!}
                                 </div>
                           </div>

                          <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right"><i class="fab fa-500px">Room Price</label></i>
                                 <div class="col-md-6">
                                 {!! Form::number('price',null,['class'=>'form-control']) !!}
                                 
                                 </div>
                           </div>

                          <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right"><i class="fas fa-address-book">Room Type</label></i>
                                 <div class="col-md-6">
                                
                                <select name="roomtype_id" class="form-control" required>
                                @foreach ($type as $item)
                                	<option value="{{$item->id}}">{{ $item->name }}</option>
                                    @endforeach
                                													
								<select>
                                
                                </div>
                           </div>

                           <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right"><i class="fas fa-door-closed">Room Amount</label></i>
                                 <div class="col-md-6">
                                    {!! Form::number('jumlah_ruangan',null,['class'=>'form-control']) !!}
                                 </div>
                           </div>

                           <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right"><i class="fas fa-door-closed">Max Orang</label></i>
                                 <div class="col-md-6">
                                    {!! Form::number('max_orang',null,['class'=>'form-control']) !!}
                                 </div>
                           </div>

                          <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Room Size</label>
                                 <div class="col-md-6">
                                 {!! Form::text('room_size',null,['class'=>'form-control']) !!}
                                 </div>
                           </div>

                           <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Facility</label>
                                 <div class="col-md-6">
                                 <textarea name="facility" id="" class="form-control"></textarea>
                                 </div>
                           </div>

                           <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Description</label>
                                 <div class="col-md-6">
                                 <textarea name="description" id="" class="form-control"></textarea>
                                 </div>
                           </div>


                           <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right"><i class="fas fa-address-book">Featured</label></i>
                                 <div class="col-md-6">
                                 {!! Form::select('featured',[1=>'Yes',0=>'No'],null,['class'=>'form-control']) !!}
                                </div>
                           </div>

                           <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Images</label>
                                 <div class="col-md-6">
                                 {!! Form::file('images',null,['class'=>'form-control']) !!}
                                 </div>
                           </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-2">
                                    <button type="submit" class="btn btn-success btn-sm">Add data</button>
                                    <a href="{{ route('room-admin.index') }}" class="btn btn-info btn-sm">Back</a>
                                </div>
                            </div>
                        </div>
                  </div>
            </div>
        </div>
    </div>
</div>
@endsection
