@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">room</div>


                   <div class="card-body">
                       <a href="{{ route('room-admin.create') }}" class="btn btn-danger btn-sm">Add room</a>

                      <hr>
                      @include('notification')

                    <table class="table table-bordered" id="users-table">
                       <thead>
                         <tr>
                            <th>No</th>
                            <th>room Name</th>
                            <th>room Type</th>
                            <th>Edit</th>
                            <th>Delete</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; ?>
                        @foreach ($room as $item)


                        <tr>
                        <td>{{ $no }}</td>
                        <td>{{ $item->room_name }}</td>
                        <td>{{ $item->type->name }}</td>
                            <td><a href="{{ route('room-admin.edit',$item->id)}}" class="btn btn-info btn-sm">Edit Data</a>
                            <a href="{{ route('edit-image-room',$item->id)}}" class="btn btn-info btn-sm">Edit Image</a>
                            </td>
                            {!! Form::open([ 'route'=>['room-admin.destroy',$item->id], 'method'=>'DELETE']) !!}
                            <td><button type="submit" name="submit" class="btn btn-danger btn-sm">Delete</button></td>
                            {!! Form::close() !!}


                        </tr>
                        <?php $no++; ?>

                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script>
$(function() {
$('#users-table').DataTable();
});
</script>
@endpush

