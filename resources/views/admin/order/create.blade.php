@extends('layouts.app')

@section('content')

<div class="container">
    <style type="text/css">
    .card-header {
        background-color: skyblue;
    }
    </style>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                  <div class="card-header">Room</div>
                      <div class="card-body">
                              @include('validate')
                          {!! Form::open(['route'=>'order.store','method'=>'POST']) !!}

                          <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right"><i class="fas fa-space-shuttle">Room Name</label></i>
                                 <div class="col-md-6">
                                 {!! Form::text('room_name',null,['class'=>'form-control']) !!}
                                 </div>
                           </div>

                          <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right"><i class="fab fa-500px">Room Price</label></i>
                                 <div class="col-md-6">
                                 {!! Form::text('room_price',null,['class'=>'form-control']) !!}
                                 </div>
                           </div>

                          <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right"><i class="fas fa-address-book">Room Type</label></i>
                                 <div class="col-md-6">
                                 {!! Form::select('room_type',['STANDART'=>'STANDART','LUXURY'=>'LUXURY'],null,['class'=>'form-control']) !!}
                                </div>
                           </div>

                           <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right"><i class="fas fa-door-closed">Room Amount</label></i>
                                 <div class="col-md-6">
                                        {!! Form::number('room_amount',null,['class'=>'form-control']) !!}
                                 </div>
                           </div>

                          <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Amount</label>
                                 <div class="col-md-6">
                                        {!! Form::number('jumlah',null,['class'=>'form-control']) !!}
                                 </div>
                           </div>



                               <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-2">
                                        <button type="submit" class="btn btn-success btn-sm">Add data</button>
                                       <a href="{{ route('order.index') }}" class="btn btn-info btn-sm">Back</a>
                                    </div>
                          </div>
                      </div>
                  </div>
            </div>
        </div>
    </div>
</div>
@endsection
