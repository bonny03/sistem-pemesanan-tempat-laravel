@extends('layouts.app')

@section('content')
<style type="text/css">
.card-header {
    background-color: aquamarine;
}
</style>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">

                <div class="card-header"><i class="fas fa-database"> Transaction Data</div></i>
                <div class="card-body">
                <hr>
                @include('notification')
                <table class="table table-bordered" id="users-table">
                    <thead>
                    <label>
                            Search:
                            <input type="search" class placeholder aria-controls="users-table">
                        <thead>
                            <tr>
                                <th><i class ="far fa-sticky-note"> No </th></i>
                                <th>User</th>
                                <th>Ruangan</th>
                                <th>Check In</th>
                                <th>Check Out</th>
                                <th>Keperluan</th>
                                <th>Jumlah Ruangan</th>
                                <th>Konfirmasi Pembayaran</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; ?>

                          @foreach ($trans as $item)
                          <tr>
                          <td>{{$no}}</td>
                          <td>{{$item->user->name}}</td>
                          <td>{{$item->room->room_name}}</td>
                          <td>{{$item->checkin}}</td>
                          <td>{{$item->checkout}}</td>
                          <td>{{$item->keperluan}}</td>
                          <td>{{ $item->jumlah_ruangan }}</td>
                          @if($item->status_bayar == 1)
                          <td><a name="submit" class="btn btn-success btn-sm">Sudah dikonfirmasi</a></td>
                          @else
                          <td><a href="{{ route('confirm-pembayaran',['id'=>$item->id])  }}" name="submit" class="btn btn-primary btn-sm">Konfirmasi Pembayaran</a></td>
                          @endif
                          
                          
                          
                          </tr>

                            <?php $no++; ?>
                          @endforeach


                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@push('scripts')
<script>
    $('#users-table').DataTable();
    });
</script>
@endpush

