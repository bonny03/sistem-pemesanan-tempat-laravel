@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                  <div class="card-header">Edit Order</div>
                      <div class="card-body">
                              @include('validate')

                              {!! Form::model($order,['route'=>['order.update',$order->id],'method'=>'PUT']) !!}

                          <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right ">Room Name</label>
                                 <div class="col-md-6">
                                 {!! Form::text('room_name',null,['class'=>'form-control']) !!}
                                 </div>
                           </div>

                          <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Room Price</label>
                                 <div class="col-md-6">
                                 {!! Form::text('room_price',null,['class'=>'form-control']) !!}
                                 </div>
                           </div>

                          <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Room Type </label>
                                 <div class="col-md-6">
                                 {!! Form::select('room_type',['STANDART'=>'STANDART','LUXURY'=>'LUXURY'],null,['class'=>'form-control']) !!}
                                </div>
                           </div>

                           <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Room Amount</label>
                                 <div class="col-md-6">
                                        {!! Form::number('room_amount',null,['class'=>'form-control']) !!}
                                 </div>
                           </div>

                          <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Amount</label>
                                 <div class="col-md-6">
                                        {!! Form::number('jumlah',null,['class'=>'form-control']) !!}
                                 </div>
                           </div>



                               <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-2">
                                        <button type="submit" class="btn btn-success btn-sm">Update Data</button>
                                       <a href="{{ route('order.index') }}" class="btn btn-info btn-sm">Back</a>
                                    </div>
                          </div>
                      </div>
                  </div>
            </div>
        </div>
    </div>
</div>

@endsection
