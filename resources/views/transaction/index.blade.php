@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
        <div class="card-header">TRANSACTION</div>
                    <div class="card-body">
                    <h3>Transaction Form</h3>
                    <table class="table table-bordered">
                          <tr><td>
                                <div class="col-md-12">
                                    <input list="tiket" name="nama_tempat" placeholder="masukan nama tempat" class="form-control">
                                </div>
                           </td></tr>
                        <tr><td>
                                <div class="col-md-12">
                                <input type="text" name="qty" placeholder="QTY" class="form-control">
                                </div>
                         </td></tr>
                        <tr><td>
                                <button type="submit" name="submit" class="btn btn-success">Simpan</button>
                                <button type="submit" name="submit" class="btn btn-danger">Selesai</button>
                            </td></tr>
                    </table>
                    </form>
                    <table class="table table-bordered">
                        <tr class="success"><th colspan="6">Detail Transaksi</th></tr>
                        <tr>
                            <th>No</th><th>Place Name</th><th>Qty</th><th>Price</th> <th>Subtotal</th><th>Cancel</th></tr>
                              <?php $no=1; $total=0; ?>
							  @foreach ($transaction as $item)
							  <tr>
                                <td>1</td>
                                <td>{{$item->id}}</td>
                                <td>{{$item->qty}}</td>
                                <td>{{$item->status}}</td>

                                <td><button type="submit" class="btn btn-danger">Cancel</button></td></tr>
								<?php $no++ ?>

								@endforeach
                                <tr><td colspan="5"><p align="right">Total</p></td><td>total</td></tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
