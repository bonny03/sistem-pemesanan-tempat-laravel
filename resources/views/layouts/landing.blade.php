<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Hotel</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/ceria.png')}}">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="{{asset('montana-master/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('montana-master/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('montana-master/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('montana-master/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('montana-master/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('montana-master/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('montana-master/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('montana-master/css/gijgo.css')}}">
    <link rel="stylesheet" href="{{asset('montana-master/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('montana-master/css/slicknav.css')}}">
    <link rel="stylesheet" href="{{asset('montana-master/css/style.css')}}">
    <!-- <link rel="stylesheet" href="css/responsive.css"> -->
    @yield('css')
</head>

<body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

    <!-- header-start -->
    <header>
        <div class="header-area ">
            <div id="sticky-header" class="main-header-area">
                <div class="container-fluid p-0">
                    <div class="row align-items-center no-gutters">
                        <div class="col-xl-5 col-lg-6">
                            <div class="main-menu  d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li><a href="{{ route('landing') }}">home</a></li>
                                        <li><a href="{{ route('room-user') }}">rooms</a></li>
                                        <!-- <li><a href="about.html">About</a></li>
                                        <li><a href="#">blog <i class="ti-angle-down"></i></a>
                                            <ul class="submenu">
                                                <li><a href="blog.html">blog</a></li>
                                                <li><a href="single-blog.html">single-blog</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">pages <i class="ti-angle-down"></i></a>
                                            <ul class="submenu">
                                                <li><a href="elements.html">elements</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="contact.html">Contact</a></li> -->
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <!-- <div class="col-xl-2 col-lg-2">
                            <div class="logo-img">
                                <a href="index.html">
                                    <img src="img/logo.png" alt="">
                                </a>
                            </div>
                        </div> -->
                        <div class="col-xl-7 col-lg-4 d-none d-lg-block">
                            @guest
                            <div class="book_room">
                                <div class="socail_links">
                                    <ul>
                                        <li>
                                            <a href="{{route('login')}}">
                                                Login
                                            </a>
                                        </li>
                                        
                                    </ul>
                                </div>
                                <div class="book_btn d-none d-lg-block">
                                    <a href="{{ route('login') }}">Book A Room</a>
                                </div>
                            </div>
                            @else
                            <div class="main-menu  d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li><a href="#">{{ Auth::user()->name }} <i class="ti-angle-down"></i></a>
                                            <ul class="submenu">
                                                <li><a href="{{ route('service-user') }}">Service</a></li>
                                                <li><a href="{{ route('logout') }}"
                              onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">{{ __('Logout') }}</a></li>
                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                            </ul>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            @endguest
                        </div>
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header-end -->

    @yield('content')

    <!-- footer -->
    <footer class="footer">
        <div class="footer_top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                About Us
                            </h3>
                            <p class="footer_text"> Hotel Mantab Ceria <br>
                                Siap melayani anda dengan sepenuh hati</p>
                            
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                address
                            </h3>
                            <p class="footer_text"> Hotel Mantab Ceria <br>
                                Suarakarta, Jawa Tengah, Indonesia</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Reservation
                            </h3>
                            <p class="footer_text">+6281123456789 <br>
                                reservation@gmail.com</p>
                        </div>
                    </div>
                    <div class="col-xl-2 col-md-6 col-lg-2">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Navigation
                            </h3>
                            <ul>
                                <li><a href="{{route('landing')}}">Home</a></li>
                                <li><a href="{{route('room-user')}}">Rooms</a></li>
                                <!-- <li><a href="#">About</a></li>
                                <li><a href="#">News</a></li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right_text">
            <div class="container">
                <div class="footer_border"></div>
                <div class="row">
                    <div class="col-xl-8 col-md-7 col-lg-9">
                        <p class="copy_right">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </div>
                    <div class="col-xl-4 col-md-5 col-lg-3">
                        <div class="socail_links">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-facebook-square"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- link that opens popup -->

    <!-- form itself end-->
        <form id="test-form" class="white-popup-block mfp-hide">
                <div class="popup_box ">
                        <div class="popup_inner">
                            <h3>Check Availability</h3>
                            <form action="#">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <input id="datepicker" placeholder="Check in date">
                                    </div>
                                    <div class="col-xl-6">
                                        <input id="datepicker2" placeholder="Check out date">
                                    </div>
                                    <div class="col-xl-6">
                                        <select class="form-select wide" id="default-select" class="">
                                            <option data-display="Adult">1</option>
                                            <option value="1">2</option>
                                            <option value="2">3</option>
                                            <option value="3">4</option>
                                        </select>
                                    </div>
                                    <div class="col-xl-6">
                                        <select class="form-select wide" id="default-select" class="">
                                            <option data-display="Children">1</option>
                                            <option value="1">2</option>
                                            <option value="2">3</option>
                                            <option value="3">4</option>
                                        </select>
                                    </div>
                                    <div class="col-xl-12">
                                        <select class="form-select wide" id="default-select" class="">
                                            <option data-display="Room type">Room type</option>
                                            <option value="1">Laxaries Rooms</option>
                                            <option value="2">Deluxe Room</option>
                                            <option value="3">Signature Room</option>
                                            <option value="4">Couple Room</option>
                                        </select>
                                    </div>
                                    <div class="col-xl-12">
                                        <button type="submit" class="boxed-btn3">Check Availability</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
            </form>
    <!-- form itself end -->

    <!-- JS here -->
    <script src="{{asset('montana-master/js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <script src="{{asset('montana-master/js/vendor/jquery-1.12.4.min.js')}}"></script>
    <script src="{{asset('montana-master/js/popper.min.js')}}"></script>
    <script src="{{asset('montana-master/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('montana-master/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('montana-master/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('montana-master/js/ajax-form.js')}}"></script>
    <script src="{{asset('montana-master/js/waypoints.min.js')}}"></script>
    <script src="{{asset('montana-master/js/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('montana-master/js/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{asset('montana-master/js/scrollIt.js')}}"></script>
    <script src="{{asset('montana-master/js/jquery.scrollUp.min.js')}}"></script>
    <script src="{{asset('montana-master/js/wow.min.js')}}"></script>
    <script src="{{asset('montana-master/js/nice-select.min.js')}}"></script>
    <script src="{{asset('montana-master/js/jquery.slicknav.min.js')}}"></script>
    <script src="{{asset('montana-master/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('montana-master/js/plugins.js')}}"></script>
    <script src="{{asset('montana-master/js/gijgo.min.js')}}"></script>

    <!--contact js-->
    <script src="{{asset('montana-master/js/contact.js')}}"></script>
    <script src="{{asset('montana-master/js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{asset('montana-master/js/jquery.form.js')}}"></script>
    <script src="{{asset('montana-master/js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('montana-master/js/mail-script.js')}}"></script>

    <script src="{{asset('montana-master/js/main.js')}}"></script>
    @yield('js')
    <script>
        $('#datepicker').datepicker({
            iconsLibrary: 'fontawesome',
            icons: {
             rightIcon: '<span class="fa fa-caret-down"></span>'
         }
        });
        $('#datepicker2').datepicker({
            iconsLibrary: 'fontawesome',
            icons: {
             rightIcon: '<span class="fa fa-caret-down"></span>'
         }

        });
    </script>
   

</body>

</html>