<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roomtype extends Model
{
    protected $table = 'room_type';
    protected $primaryKey = 'id';
    protected $fillable = ['name','description'];

    public function room()
    {
        return $this->hasMany('App\Room');
    }
}
