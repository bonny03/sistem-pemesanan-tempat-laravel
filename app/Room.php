<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'room';
    protected $primaryKey = 'id';
    protected $fillable = ['roomtype_id','room_name','room_size','price','jumlah_ruangan','max_orang','facility','description','featured','images'];

    public function order()
    {
        return $this->hasMany('App\Order');
    }

    public function type()
    {
        return $this->belongsTo('App\Roomtype', 'roomtype_id');
    }
}
