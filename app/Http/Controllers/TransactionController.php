<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;

class TransactionController extends Controller
{

     public function index()

     {
         $transaction=Transaction::where('status','0')->get();

         return view('transaction.index',compact('transaction'));
     }

}
