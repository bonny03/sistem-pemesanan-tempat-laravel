<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Room;
use App\Roomtype;
use App\Order;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['room']=Room::all();
        $data['featured']=Room::where('featured',true)->get();
        return view('user.room',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data['room'] = Room::find($request->id);
        $data['dstart'] = date("Y-m-d");
        $data['dend'] = date('Y-m-d', strtotime("+1 months", strtotime( $data['dstart'])));
        $data['num_days'] = floor((strtotime($data['dend'])-strtotime($data['dstart']))/(60*60*24));
        $data['batas_bayar'] = date('Y-m-d H:i:s', strtotime("+0.5 hours", strtotime( $data['dstart'])));
        $data['jam_sekarang'] = date("Y-m-d H:i:s");
        // $data['order'] = Order::select('checkin','checkout')->get();
        $data['order']  = array();
        $data['sisa']  = array();
        for($i=0; $i<$data['num_days']; $i++){
            $hari = strtotime($data['dstart'] . "+ $i days");
            $cek = Order::where(
                [
                    ['room_id','=',$request->id],
                    ['checkin','<=',date('Y-m-d',$hari)],
                    ['checkout','>=',date('Y-m-d',$hari)],
                ]
            )->get();
            if(count($cek)>0){
                $jml_terpakai = 0;
                foreach ($cek as $key) {
                    $jml_terpakai  =  $jml_terpakai + $key->jumlah_ruangan;
                }
                if($data['room']->jumlah_ruangan == $jml_terpakai){
                    array_push($data['order'],date('Y-m-d',$hari));
                    array_push($data['sisa'],0);
                }
                else{
                    array_push($data['sisa'],$data['room']->jumlah_ruangan-$jml_terpakai);
                }
            }
            else{
                array_push($data['sisa'],$data['room']->jumlah_ruangan);
            }
           
        }
        // dd($data['order']);
        // dd($data['sisa']);
        return view('user.book',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::id();
        $room = $request->id;
        $rooms = Room::find($room);
        $jumlah_orang = $request->input('jumlah_orang');
        $jumlah_ruang = $request->input('jumlah_ruangan');
        $jumlah_maks_orang = $rooms->max_orang * $jumlah_ruang;
        $lama_peminjaman =  $data['num_days'] = floor((strtotime($request->input('checkout'))-strtotime($request->input('checkin')))/(60*60*24));
        $harga = intval($lama_peminjaman+1)*$rooms->price*$jumlah_ruang;
        // dd($harga);
        if($jumlah_maks_orang >= $jumlah_orang && $rooms->jumlah_ruangan >= $jumlah_ruang){
            $book = Order::firstorCreate(
                array(
                    'user_id' => $user,
                    'room_id' => $room,
                    'keperluan' => $request->input('keperluan'),
                    'jumlah_ruangan' => $request->input('jumlah_ruangan'),
                    'jumlah_orang' => $request->input('jumlah_orang'),
                    'checkin' => $request->input('checkin'),
                    'checkout' => $request->input('checkout'),
                    'no_wa' => $request->input('no_wa'),
                    'status_checkout' => false,
                    'status_bayar' => false,
                    'total_harga' => $harga,       
                )
            );
                // dd($request->input());
            return redirect()->route('service-user')->with('pesan', 'data entered successfully');
        }
        else{
            return redirect()->back()->with('pesan', 'maaf jumlah ruang tidak cukup atau jumlah orang terlalu banyak');
        }
        
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
    
    public function search(Request $request)
    {
        $search = $request->input('search');
        $data['room'] = Room::where('room_name','like',"%".$search."%")->orWhere('price','like',"%".$search."%")->get();
        $data['featured'] = Room::where('room_name','like',"%".$search."%")->orWhere('price','like',"%".$search."%")->where('featured',true)->get();
        return view('user.search', $data);
    }

    public function type(Request $request)
    {
        $data['type'] = Roomtype::find($request->id);
        return view('user.type',$data);
    }
}
