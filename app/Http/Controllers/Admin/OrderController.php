<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use App\transaction;
use App\Room;
use Spatie\GoogleCalendar\Event;
use Carbon\Carbon;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $order=Order::all();
        return view('admin.order.index',compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([

        //     'room_name' => 'required|min:4',
        //     'room_price' =>'required|numeric',
        //     'room_amount' =>'required',
        //     'room_type' =>'required',
        // ]);

        // Order::create($request->all());
        // return redirect()->route('order.index')->with('pesan', 'data entered successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $order = Order::findOrFail($id);
        // return view('order.edit',compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $request->validate([

        //     'room_name' => 'required|min:4',
        //     'room_price' =>'required|numeric',
        //     'room_amount' =>'required',
        //     'room_type' =>'required',
        // ]);

        // Order::create($request->all());
        // return redirect()->route('order.index')->with('pesan', 'data entered successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order=Order::find($id);
        $order->delete();
        return redirect()->route('order.index')->with('pesan','data deleted successfully');
    }

    public function transaction()
    {
        $data['trans']=Order::get();
        return view('admin.order.transaction',$data);
    }
    
    public function confirmPembayaran(Request $request)
    {
        $order = Order::find($request->id); 
        $order->status_bayar = 1;
        $order->save();

        $event = new Event;
        $event->name = $order->keperluan;
        $event->startDateTime = Carbon::parse($order->checkin);
        $event->endDateTime = Carbon::parse($order->checkout);
        $event->save();

        return back();
    }

    public function checkout()
    {
        
    }
}
