<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Room;
use App\Roomtype;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;

class RoomController extends Controller
{
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['room']=Room::all();
        return view('admin.room.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = Roomtype::all();
        return view('admin.room.create',compact('type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'roomtype_id' => 'required',
            'room_name' => 'required',
            'room_size' => 'required',
            'price' => 'required|numeric',
            'jumlah_ruangan' => 'required|numeric',
            'max_orang' => 'required|numeric',
            'facility' => 'required',
            'description' => 'required',
            'images' => 'mimes:jpeg,jpg,png|max:1000',
        ]);
        
        $file_room = $request->file('images');
        $nama_file_room = time()."_".$file_room->getClientOriginalName();
        $tujuan_upload_room = 'room-images';
        $file_room->move($tujuan_upload_room,$nama_file_room);
        $book = Room::firstorCreate(
            array(
                'roomtype_id' => $request->input('roomtype_id'),
                'room_name' => $request->input('room_name'),
                'room_size' => $request->input('room_size'),
                'price' => $request->input('price'),
                'jumlah_ruangan' => $request->input('jumlah_ruangan'),
                'max_orang' => $request->input('max_orang'),
                'facility' => $request->input('facility'),
                'description' => $request->input('description'),
                'images' => $nama_file_room,
                'featured' => $request->input('featured'),       
            )
        );
        return redirect()->route('room-admin.index')->with('pesan', 'data entered successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = Roomtype::all();
        $room = Room::findOrFail($id);
        return view('admin.room.edit',compact('room'),compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'roomtype_id' => 'required',
            'room_name' => 'required',
            'room_size' => 'required',
            'price' => 'required|numeric',
            'jumlah_ruangan' => 'required|numeric',
            'max_orang' => 'required|numeric',
            'facility' => 'required',
            'description' => 'required'
        ]);

        $room=Room::find($id);
        $room->update($request->all());
        return redirect()->route('room-admin.index')->with('pesan', 'data entered successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $room=room::find($id);
        $room->delete();
        return redirect()->route('admin.room.index')->with('pesan','data deleted successfully');
    }

    public function editImages($id)
    {
        $room = Room::findOrFail($id);
        return view('admin.room.editimages',compact('room'));
    }

    public function updateImages(Request $request)
    {
        $room_id = $request->id;
        $request->validate([

            'images' => 'mimes:jpeg,jpg,png|max:1000',
        ]);
        
        $file_room = $request->file('images');
        $nama_file_room = time()."_".$file_room->getClientOriginalName();
        $tujuan_upload_room = 'room-images';
        $file_room->move($tujuan_upload_room,$nama_file_room);

        $image = Room::where('id', '=', $room_id)->update(
            array(
                'images' => $nama_file_room,
                
            )
        );
        return redirect()->route('room-admin.index')->with('pesan', 'data entered successfully');
    }
}
