<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('room_id')->index();
            $table->string('keperluan');
            $table->integer('jumlah_ruangan');
            $table->integer('jumlah_orang');
            $table->date('checkin');
            $table->date('checkout');
            $table->unsignedBigInteger('no_wa');
            $table->boolean('status_checkout')->default(false);
            $table->boolean('status_bayar')->default(false);
            $table->integer('total_harga');
            
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('room_id')
            ->references('id')->on('room')
            ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
