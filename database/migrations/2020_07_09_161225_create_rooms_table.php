<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('roomtype_id')->index();
            $table->string('room_name');
            $table->string('room_size');
            $table->integer('price');
            $table->integer('jumlah_ruangan');
            $table->integer('max_orang');
            $table->longText('facility');
            $table->longText('description');
            $table->string('images')->nullable();
            $table->boolean('featured')->default(false);

            $table->foreign('roomtype_id')
            ->references('id')->on('room_type')
            ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
