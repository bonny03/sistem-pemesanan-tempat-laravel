<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            ['name'=>'admin'],
            ['name'=>'user'],
        ]);

        DB::table('users')->insert([
            ['name'=>'user',
            'email'=>'user@gmail.com',
            'role_id'=>2,
            'password'=>'$2y$10$2KoHuYaZUSL5dLb49DR8Z.ZcHLgqyfchlJ2c14NcZpiwfmNdWDAdC',
            ],
            ['name'=>'admin',
            'email'=>'admin@gmail.com',
            'role_id'=>1,
            'password'=>'$2y$10$2KoHuYaZUSL5dLb49DR8Z.ZcHLgqyfchlJ2c14NcZpiwfmNdWDAdC',
            ],
        ]);
        DB::table('room_type')->insert([
            ['name'=>'Luxury',
            'description'=>'Luxury Mantab',
            ],
            ['name'=>'Hall',
            'description'=>'Hall Room Mantab',
            ],
        ]);
        DB::table('room')->insert([
            ['roomtype_id'=>'1',
            'room_name'=>'Luxury Mantab',
            'room_size'=>'4x5',
            'price'=>'100000',
            'jumlah_ruangan'=>'10',
            'max_orang'=>'6',
            'facility'=>'AC, Kasur',
            'description'=>'Luxury Mantab',
            'images'=>'1.png',
            'featured'=>false,
            ],
            ['roomtype_id'=>'2',
            'room_name'=>'Ball Room',
            'room_size'=>'20x25',
            'price'=>'1500000',
            'jumlah_ruangan'=>'1',
            'max_orang'=>'100',
            'facility'=>'AC, Pengeras Suara, Kursi',
            'description'=>'Ball Room buat kumpul',
            'images'=>'3.png',
            'featured'=>true,
            ],
        ]);
        DB::table('orders')->insert([
            ['user_id'=>'1',
            'room_id'=>'1',
            'keperluan'=>'menginap',
            'jumlah_ruangan'=>'1',
            'jumlah_orang'=>'5',
            'checkin'=>'2020-07-03',
            'checkout'=>'2020-07-10',
            'no_wa'=>'086876868',
            'status_checkout'=>false,
            'status_bayar'=>false,
            'total_harga'=>'100000',
            ],
        ]);
    }
}
