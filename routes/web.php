<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['auth']], function () {
    Route::resource('place', 'PlaceController');
    //Route::resource('data', 'DataController');
  
    Route::resource('room', 'User\RoomController');
    Route::get('/room-book/{id}', 'User\RoomController@create')->name('room-book');
    Route::get('/service-user', 'User\UserController@service')->name('service-user');
    
    Route::get('/search', 'User\RoomController@search')->name('search');
    Route::get('/type/{id}', 'User\RoomController@type')->name('type');
});

Route::group(['middleware' => ['auth','admin']], function () {
    Route::resource('place', 'PlaceController');
    //Route::resource('data', 'DataController');
    Route::resource('order-admin', 'Admin\OrderController');
    Route::resource('room-admin', 'Admin\RoomController');
    Route::resource('roomtype-admin', 'Admin\RoomtypeController');
 
    Route::get('/confirm-pembayaran', 'Admin\OrderController@confirmPembayaran')->name('confirm-pembayaran');
    Route::get('/checkout-admin', 'Admin\OrderController@checkout')->name('checkout');
    Route::get('/transaction-admin', 'Admin\OrderController@transaction')->name('transaction-admin');
    Route::get('/edit-image-room/{id}', 'Admin\RoomController@editImages')->name('edit-image-room');
    Route::post('/edit-image-room-admin', 'Admin\RoomController@updateImages')->name('edit-image-room-admin');
});

Route::get('/landing', 'User\UserController@index')->name('landing');
Route::get('/', function () {
    return redirect()->route('landing');
});
Route::get('/room-user', 'User\RoomController@index')->name('room-user');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/transaction','TransactionController@index')->name('transaction.index');
